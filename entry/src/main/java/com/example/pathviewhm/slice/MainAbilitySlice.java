/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.pathviewhm.slice;

import com.example.pathviewhm.ResourceTable;
import com.example.pvlib.pathView.PathView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainAbilitySlice extends AbilitySlice {

    PathView pathView;
    ComponentContainer roots;
    private boolean showIcon = true;
    private boolean hideIcon = true;
    private static final int TEN = 10;
    private static final int HUNDRED= 100;
    private static final int TWOHUNDRED = 200;
    private static final int THOUSAND = 1000;
    private static final int THOUSANDFIVE = 1500;
    private static final int THREETHOUSAND = 3000;
    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MainAbilitySlice");


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        roots = (ComponentContainer) findComponentById(ResourceTable.Id_root);
        Text show = (Text) findComponentById(ResourceTable.Id_showSvg);
        Text hide = (Text) findComponentById(ResourceTable.Id_hideSvg);
        show.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!showIcon && !hideIcon) {
                    new ToastDialog(getContext()).setText("请不要重复点击").setDuration(TWOHUNDRED).setAlignment(LayoutAlignment.CENTER).show();
                } else {  showIcon = false; hideIcon = false;  pathView = new PathView(getContext());
                    DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(THOUSAND, THOUSAND);
                    pathView.setLayoutConfig(config); pathView.setFill(true);
                    pathView.getPathAnimator().delay(HUNDRED).duration(THOUSANDFIVE).start();
                    pathView.setSvgResource("resources/rawfile/monitor.xml"); roots.addComponent(pathView);
                    ExecutorService executorService = Executors.newFixedThreadPool(TEN);
                    executorService.submit(new Runnable(){
                        @Override
                        public void run() {
                            try {Thread.sleep(THREETHOUSAND); hideIcon = true;showIcon = true;roots.removeAllComponents();roots.invalidate();
                            } catch (InterruptedException e) {HiLog.error(LABEL,"MainAbilitySlice", e);}
                        }
                    }); } }
        });
        hide.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!showIcon && !hideIcon) {
                    new ToastDialog(getContext()).setText("请不要重复点击").setDuration(TWOHUNDRED).setAlignment(LayoutAlignment.CENTER).show();
                } else {
                    showIcon = false;hideIcon = false; pathView = new PathView(getContext());
                    DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(THOUSAND, THOUSAND);
                    pathView.setLayoutConfig(config); pathView.setFill(false);
                    pathView.getPathAnimator().delay(HUNDRED).duration(THOUSANDFIVE).start();
                    pathView.setSvgResource("resources/rawfile/monitor.xml"); roots.addComponent(pathView);
                    ExecutorService executorService = Executors.newFixedThreadPool(TEN);
                    executorService.submit(new Runnable(){
                        @Override
                        public void run() {
                            try { Thread.sleep(THREETHOUSAND);hideIcon = true; showIcon = true; roots.removeAllComponents(); roots.invalidate();
                            } catch (InterruptedException e) { HiLog.error(LABEL,"MainAbilitySlice", e);}
                        }
                    });}}
        });
    }

    @Override
    public void onActive() {
        super.onActive();

    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

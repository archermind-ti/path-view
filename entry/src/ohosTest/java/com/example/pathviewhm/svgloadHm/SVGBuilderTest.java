package com.example.pathviewhm.svgloadHm;

import com.example.pvlib.svgloadHm.SVGBuilder;
import ohos.agp.render.ColorFilter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

public class SVGBuilderTest {

    private final SVGBuilder svgBuilder = new SVGBuilder();
    private InputStream inputStream;
    private Object blendMode;

    @Before
    public void setUp() throws Exception {
        inputStream = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
    }

    @Test
    public void readFromInputStream() {
        SVGBuilder svg = svgBuilder.readFromInputStream(inputStream);
        assertSame(svgBuilder, svg);
    }

    @Test
    public void readFromString() {
        SVGBuilder svg = svgBuilder.readFromString("svgData");
        assertSame(svgBuilder, svg);
    }

    @Test
    public void clearColorSwap() {
        SVGBuilder svg = svgBuilder.clearColorSwap();
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setColorSwap() {
        SVGBuilder svg = svgBuilder.setColorSwap(1,0);
        assertSame(svgBuilder, svg);
    }

    @Test
    public void testSetColorSwap() {
        SVGBuilder svg = svgBuilder.setColorSwap(1,0, true);
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setWhiteMode() {
        SVGBuilder svg = svgBuilder.setWhiteMode(true);
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setColorFilter() {
        SVGBuilder svg = svgBuilder.setColorFilter(new ColorFilter(1, (ohos.agp.render.BlendMode) blendMode));
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setStrokeColorFilter() {
        SVGBuilder svg = svgBuilder.setStrokeColorFilter(new ColorFilter(1, (ohos.agp.render.BlendMode) blendMode));
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setFillColorFilter() {
        SVGBuilder svg = svgBuilder.setFillColorFilter(new ColorFilter(1, (ohos.agp.render.BlendMode) blendMode));
        assertSame(svgBuilder, svg);
    }

    @Test
    public void setCloseInputStreamWhenDone() {
        SVGBuilder svg = svgBuilder.setCloseInputStreamWhenDone(true);
        assertSame(svgBuilder, svg);
    }
}
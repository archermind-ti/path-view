package com.example.pathviewhm.pathView;

import com.example.pvlib.pathView.PathView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.render.Path;
import ohos.app.Context;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PathViewTest {

    private final Context mContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    private final PathView pathView = new PathView(mContext);

    @Test
    public void setPaths() {
        List<Path> newList = new ArrayList<>(2);
        newList.add(new Path());
        newList.add(new Path());
        pathView.setPaths(newList);
        assertEquals(2, pathView.paths.size());
    }

    @Test
    public void setPath() {
        pathView.setPath(new Path());
        assertEquals(1, pathView.paths.size());
    }

    @Test
    public void setPathColor() {
        pathView.setPathColor(0);
        int pathColor = pathView.getPathColor();
        assertEquals(0, pathColor);
    }

    @Test
    public void setPathWidth() {
        pathView.setPathWidth(1);
        int pathWidth = (int) pathView.getPathWidth();
        assertEquals(1, pathWidth);
    }

    @Test
    public void getSvgResource() {
    }

    @Test
    public void setSvgResource() {
        pathView.setSvgResource("1");
        String svgResource = pathView.getSvgResource();
        assertEquals("1", svgResource);
    }
}
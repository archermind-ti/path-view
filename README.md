# pathView

- 读取web版svg文件（根节点为svg）并通过path measure对path路径加载设置动画


### 项目介绍

- com.example.pathviewhm 包为demo
- pathView包，svgloadHm包 为核心功能
- pathView .Animator 包 为pathview控件动画控制类
- svgloadHm包 负责将web版svg xml文件的解析成java对象 
- PathViewDemo类 为自定义控件 继承与Image 可以加载svg图片 并已动画的形式显示SVG
- SvgUtils 类 为PathviewDemo相关功能的帮助类  


### 集成
##### 方法1: 直接使用har包
```
通过library生成har包，添加har包到要集成的libs文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

##### 方法2: gradle依赖
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'com.gitee.archermind-ti:ohos-pathview:1.0.0-beta2'

```


#### 效果动图

<img src="/screenShoot/demonstration.gif" width="300" height="569"/>

#### 使用说明

1动态创建pathviewDemo控件

  ```
super.setUIContent(ResourceTable.Layout_ability_main);
root = (ComponentContainer) findComponentById(ResourceTable.Id_root);pathView=new PathViewDemo(getContext());
DirectionalLayout.LayoutConfig config = newDirectionalLayout.LayoutConfig(1000,1000);
pathView.setLayoutConfig(config);
root.addComponent(pathView);

  ```

2设置动画

  ``` 
pathView.getPathAnimator().       
                      delay(100).
                   duration(1500).
                        start();
  ```

3 设置动画完成后显示原svg图


  ```
pathView.setFill(true);
  ```

##### Licence

    Copyright 2016 Georgi Eftimov

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.pvlib.pathView.Animator;

import com.example.pvlib.pathView.PathView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;


/**
 * Object for building the animation of the path of this view.
 */
public class AnimatorBuilder {
    /**
     * Duration of the animation.
     */
    private int duration = 350;
    /**
     * Interpolator for the time of the animation.
     */
    // private Interpolator interpolator;
    /**
     * The delay before the animation.
     */
    private int delay = 0;
    /**
     * ObjectAnimator that constructs the animation.
     */
    //  private final ObjectAnimator anim;
    private final AnimatorValue anim;
    /**
     * Listener called before the animation.
     */
    private ListenerStart listenerStart;
    /**
     * Listener after the animation.
     */
    private ListenerEnd animationEnd;
    /**
     * Animation listener.
     */
    private PathViewAnimatorListener pathViewAnimatorListener;

    /**
     * Default constructor.
     *
     * @param pathView The view that must be animated.
     */
    public AnimatorBuilder(final PathView pathView) {
        // anim = ObjectAnimator.ofFloat(pathView, "percentage", 0.0f, 1.0f);
        anim = new AnimatorValue();
        anim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {

                pathView.setPercentage(v);
               // ToastUtil.makeToast(pathView.getContext(), v + "");
            }
        });
    }

    /**
     * Set the duration of the animation.
     *
     * @param duration - The duration of the animation.
     * @return AnimatorBuilder.
     */
    public AnimatorBuilder duration(final int duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Set the Interpolator.
     *
     * @param interpolator - Interpolator.
     * @return AnimatorBuilder.
     */
    //取消了插值器
/*
    public AnimatorBuilder interpolator(final Interpolator interpolator) {
        this.interpolator = interpolator;
        return this;
    }
*/

    /**
     * The delay before the animation.
     *
     * @param delay - int the delay
     * @return AnimatorBuilder.
     */
    public AnimatorBuilder delay(final int delay) {
        this.delay = delay;
        return this;
    }

    /**
     * Set a listener before the start of the animation.
     *
     * @param listenerStart an interface called before the animation
     * @return AnimatorBuilder.
     */
    public AnimatorBuilder listenerStart(final ListenerStart listenerStart) {
        this.listenerStart = listenerStart;
        if (pathViewAnimatorListener == null) {
            pathViewAnimatorListener = new PathViewAnimatorListener();
            //anim.addListener(pathViewAnimatorListener);
        }
        return this;
    }

    /**
     * Set a listener after of the animation.
     *
     * @param animationEnd an interface called after the animation
     * @return AnimatorBuilder.
     */
    public AnimatorBuilder listenerEnd(final ListenerEnd animationEnd) {
        this.animationEnd = animationEnd;
        if (pathViewAnimatorListener == null) {
            pathViewAnimatorListener = new PathViewAnimatorListener();
            //anim.addListener(pathViewAnimatorListener);
            anim.setStateChangedListener(pathViewAnimatorListener);
        }
        return this;
    }

    /**
     * Starts the animation.
     */
    public void start() {
        anim.setDuration(duration);
        //anim.setInterpolator(interpolator);
        anim.setDelay(delay);
        anim.start();
    }

    /**
     * Animation listener to be able to provide callbacks for the caller.
     */
    private class PathViewAnimatorListener implements Animator.StateChangedListener {

        @Override
        public void onStart(Animator animator) {
            if (listenerStart != null)
                listenerStart.onAnimationStart();
        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {

        }

        @Override
        public void onEnd(Animator animator) {
            if (animationEnd != null)
                animationEnd.onAnimationEnd();
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    }
 /*       private class PathViewAnimatorListener implements Animator.StateChangedListener {

            @Override
            public void onAnimationStart(Animator animation) {
                if (listenerStart != null)
                    listenerStart.onAnimationStart();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (animationEnd != null)
                    animationEnd.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }*/

    /**
     * Called when the animation start.
     */
    public interface ListenerStart {
        /**
         * Called when the path animation start.
         */
        void onAnimationStart();
    }

    /**
     * Called when the animation end.
     */
    public interface ListenerEnd {
        /**
         * Called when the path animation end.
         */
        void onAnimationEnd();
    }
}
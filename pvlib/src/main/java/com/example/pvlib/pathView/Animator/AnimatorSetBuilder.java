/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.pvlib.pathView.Animator;

import com.example.pvlib.pathView.PathView;
import com.example.pvlib.pathView.SvgUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;


import java.util.ArrayList;
import java.util.List;

/**
 * Object for building the sequential animation of the paths of this view.
 */
public class AnimatorSetBuilder {

    /**
     * Duration of the animation.
     */
    private int duration = 1000;
    /**
     * Interpolator for the time of the animation.
     */
    //  private Interpolator interpolator;
    /**
     * The delay before the animation.
     */
    private int delay = 0;
    /**
     * List of ObjectAnimator that constructs the animations of each path.
     */
    // private final List<Animator> animators = new ArrayList<>();
    private final List<AnimatorValue> animators = new ArrayList<>();
    /**
     * Listener called before the animation.
     */
    private AnimatorBuilder.ListenerStart listenerStart;
    /**
     * Listener after the animation.
     */
    private AnimatorBuilder.ListenerEnd animationEnd;
    /**
     * Animation listener.
     */
    private PathViewAnimatorListener pathViewAnimatorListener;
    /**
     * The animator that can animate paths sequentially
     */
    //animatorSet = new AnimatorSet();
    private AnimatorGroup animatorGroup = new AnimatorGroup();
    /**
     * The list of paths to be animated.
     */
    private List<SvgUtils.SvgPath> paths;

    /**
     * Default constructor.
     *
     * @param pathView The view that must be animated.
     */
    public AnimatorSetBuilder(final PathView pathView) {
        paths = pathView.paths;
        for (SvgUtils.SvgPath path : paths) {
            //     path.setAnimationStepListener(pathView);
            // ObjectAnimator animation = ObjectAnimator.ofFloat(path, "length", 0.0f, path.getLength());
            AnimatorValue animavalue = new AnimatorValue();
            animavalue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {

                }
            });
            animators.add(animavalue);
        }
        //animatorSet.playSequentially(animators);

        animatorGroup.runSerially((Animator[]) animators.toArray());
    }

    /**
     * Sets the duration of the animation. Since the AnimatorSet sets the duration for each
     * Animator, we have to divide it by the number of paths.
     *
     * @param duration - The duration of the animation.
     * @return AnimatorSetBuilder.
     */
    public AnimatorSetBuilder duration(final int duration) {
        this.duration = duration / paths.size();
        return this;
    }

    /**
     * Set the Interpolator.
     * <p>
     * //  * @param interpolator - Interpolator.
     *
     * @return AnimatorSetBuilder.
     */
    //取消了插值器
  /*  public AnimatorSetBuilder interpolator(final Interpolator interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    /**
     * The delay before the animation.
     *
     * @param delay - int the delay
     * @return AnimatorSetBuilder.
     */
    public AnimatorSetBuilder delay(final int delay) {
        this.delay = delay;
        return this;
    }

    /**
     * Set a listener before the start of the animation.
     *
     * @param listenerStart an interface called before the animation
     * @return AnimatorSetBuilder.
     */
    public AnimatorSetBuilder listenerStart(final AnimatorBuilder.ListenerStart listenerStart) {
        this.listenerStart = listenerStart;
        if (pathViewAnimatorListener == null) {
            pathViewAnimatorListener = new PathViewAnimatorListener();
            // anima.addListener(pathViewAnimatorListener);

        }
        return this;
    }

    /**
     * Set a listener after of the animation.
     *
     * @param animationEnd an interface called after the animation
     * @return AnimatorSetBuilder.
     */
    public AnimatorSetBuilder listenerEnd(final AnimatorBuilder.ListenerEnd animationEnd) {
        this.animationEnd = animationEnd;
        if (pathViewAnimatorListener == null) {
            pathViewAnimatorListener = new PathViewAnimatorListener();
            animatorGroup.setStateChangedListener(pathViewAnimatorListener);

        }
        return this;
    }

    /**
     * Starts the animation.
     */
    public void start() {
        resetAllPaths();
           /* animatorSet.cancel();
            animatorSet.setDuration(duration);
            animatorSet.setInterpolator(interpolator);
            animatorSet.setStartDelay(delay);
            animatorSet.start();*/
        animatorGroup.cancel();
        animatorGroup.setDuration(duration);
        //animatorGroup.setInterpolator(interpolator);
        animatorGroup.setDelay(delay);
        animatorGroup.start();

    }

    /**
     * Sets the length of all the paths to 0.
     */
    private void resetAllPaths() {
        for (SvgUtils.SvgPath path : paths) {
            path.setLength(0);
        }
    }

    /**
     * Animation listener to be able to provide callbacks for the caller.
     */
      /*  private class PathViewAnimatorListener implements Animator.AnimatorListener {

            @Override
            public void onAnimationStart(Animator animation) {
                if (listenerStart != null)
                    listenerStart.onAnimationStart();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (animationEnd != null)
                    animationEnd.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }*/
    private class PathViewAnimatorListener implements Animator.StateChangedListener {

        @Override
        public void onStart(Animator animator) {
            if (listenerStart != null)
                listenerStart.onAnimationStart();
        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {

        }

        @Override
        public void onEnd(Animator animator) {
            if (animationEnd != null)
                animationEnd.onAnimationEnd();
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    }
}

/**
 * objects. This allows vector graphics files to be saved out of illustration software (such as Adobe Illustrator) as
 * SVG Basic and then used directly in an app.
 * <p/>
 * The following SVG Basic 1.1 features are not supported and will be ignored by the parser:
 * <ul>
 * <li>All text and font features.
 * <li>Styles.
 * <li>Symbols, conditional processing.
 * <li>Patterns.
 * <li>Masks, filters and views.
 * <li>Interactivity, linking, scripting and animation.
 * </ul>
 * Even with the above features missing, users will find that most Illustrator drawings will render perfectly well on
 * library.
 */
package com.example.pvlib.svgloadHm;

